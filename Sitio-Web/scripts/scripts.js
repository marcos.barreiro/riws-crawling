////////////////////////////////////////////
// --------------- GLOBALES --------------//
////////////////////////////////////////////
$( document ).ready(function() {
    window.scriptLoaded = 
    count = 0;
    llenarCard = function(titulo, fecha, breve, descripcion, edicion, imagen, palabras, link) {
        var edition = ''
        switch (edicion) {
            case 'co':
                edition = '(CO) Colombia';
                break;
            case 'ar':
                edition = '(AR) Argentina';
                break;
            case 'mx':
                edition = '(MX) México';
                break;
            case 'us':
                edition = '(US) USA';
                break;
            case 'es':
                edition = '(ES) España';
                break;
        }
        var date = new Date(fecha);
        var id = "id";
        card = '<section id="noticia" role="tablist" aria-multiselectable="true" class="noticia"><div class="card"><header class="card-header" role="tab" id="encabezado"><h5 class="mb-0"><button class="btn btn-light btn-block btn-card" data-toggle="collapse"data-target="#'+id+count+'" aria-expanded="true" aria-controls="'+id+count+'"><div class="d-flex justify-content-between "><span id="titulo-noticia" class="font-weight-bold">'+titulo+'</span><span id="fecha-noticia" class="text-muted">'+ date.getHours() +':'+ date.getMinutes() + ' ' + date.getDate() + '/' + date.getMonth() + '/' + date.getFullYear()+'</span></div><hr class="my-0" /><aside class="text-justify"><span class="breve-noticia">'+breve+'</span></aside></button></h5></header><div id="'+id+count+'" class="collapse in" role="tabpanel" aria-labelledby="encabezado"><div class="card-body d-flex"><section class="col-8" id="descripcion-noticia">'+descripcion+'</section><section class="text-right col-4"><p class="text-justify" id="edicion-noticia"><strong>Edición: </strong>'+edicion+'</p><figure><img class="img-fluid" src="'+imagen+'" alt="Imagen de la noticia" id="imagen-noticia"></figure></section></div><section class="palabrasNoticia"><p>Palabras clave: <span class="text-muted" id="palabras-noticia">'+palabras+'</span></p></section><hr class="my-0"><section class="text-center"><a id="link-noticia" href="'+link+'" class="badge badge-pill badge-secondary btn-block">Ver noticia en marca.com</a></section></div></div></section>'
        $("#noticias").append(card);
        count+=1;
    };
    
   search = function(){
        URL = "http://localhost:8983/solr/collection1/";
        RH = "select?q="
        ident = "&indent=true"
        wt = '&wt=json'
        count = 0;
        
        var query = ''
        $('#noticias').empty()
        
        //Check title
        var title = $('#title').val();
        if (title !=''){
            query += 'title%3A' + title
        }
        
        //Check date
        var date = $('#date').val();
        if (date !=''){
            if (query != ''){
                query += '+AND+meta_date%3A%5B'+ date + 'T00%3A00%3A00Z TO '+ date +'T23%3A59%3A59Z%5D'
            }else{
                query += 'meta_date%3A%5B'+ date + 'T00%3A00%3A00Z TO ' + date + 'T23%3A59%3A59Z%5D'
            }
        }
        
        //Check keywords
        var keywords = $('#keywords').val();
        if (keywords != ''){
            var keywordsList = keywords.split(',');
            for (var i = 0; i < keywordsList.length; i++){
                if (query != ''){
                    query += '+AND+meta_keywords%3A",' + keywordsList[i]+ '"'
                }else {
                    query += 'meta_keywords%3A",' + keywordsList[i] + '"'
                }
            }
        }
        //Check edition
        var edition = $('#edition').val();
        if (edition != ''){
            if (query != ''){
                query += '+AND+edition%3A' + edition
            }else {
                query += 'edition%3A' + edition
            }
        }
        //Check lang
        var lang =  $('input[name=idiomas]:checked', '#searchForm').val()
        if (edition == 'es'){
             if (query != ''){
                query += '+AND+lang%3A' + lang
            }else {
                query += 'lang%3A' + lang
            }
        }
        
        if (query == ''){
            query = '*'
        }
        
        //Check order
        query += '&sort=meta_date+' + $('input[name=order]:checked', '#searchForm').val()
        
        //Check amount
        var amount = 10;
        if ($('#rows').val() != ''){
            amount = $('#rows').val()
        }
        query += '&rows='+amount;
        
        var url = URL + RH + query + ident + wt
        console.log(url);
        $.ajax({
            url: URL + RH + query + ident + wt,
            dataType: 'jsonp',
            type: 'GET',
            responseType:'application/json',
            jsonp: 'json.wrf',
            date: {},
            success: function(response){
                if(response.response.docs.length == 0){
                    $("#noticias").append('<div class="alert alert-primary" role="alert"><h2>No se han encontrado resultados a la búsqueda. Pruebe con otros parámetros</h2></div>');
                    return;
                }else{
                    $.each(response.response.docs, function(key,value){
                        llenarCard(value.title, value.meta_date, value.meta_description, value.content, value.edition, value.meta_image, value.meta_keywords, value.url);
                    })
                }
            },
            error: function(response){
                $("#noticias").append('<div class="alert alert-danger" role="alert">' + response.statusText + ' ' + response.status + '</div>')
            }
        })
    };
});
