# Práctica RIWS - Crawling

#### Mestrado Universitario en Enxeñería Informática


Autores:
* Enrique Rodríguez Castañeda
* Marcos Barreiro Otero

Pasos para cargar la práctica:

1.  Arrancar solr
```sh
cd solr-4.8.1/example
java -Dsolr.solr.home=solr -jar start.jar
```
2.  Acceder a la interfaz web: abrir con un navegador el archivo **index.html** en la carpeta Sitio-Web.

En el repositiorio ya se encuentra una batería de datos variados para probar todos las variantes de la query. En caso de querer modificar los datos que se encuentran en los indices hay que seguir los siguientes pasos:

1. Arrancar solr: de la misma forma que en el paso 1 explicado anteriormente.
2. Vaciar los índices de solr: un método es accediendo con el navegador a [http://localhost:8983/solr/collection1/update?stream.body=<delete><query>*:*</query></delete>&commit=true](http://localhost:8983/solr/collection1/update?stream.body=<delete><query>*:*</query></delete>&commit=true).
3. Arrancar hbase: mediante el comando './hbase-0.94.14/bin/start-hbase.sh'.
4. Acceder a la carpeta runtime loca de nutch `cd apache-nutch-2.3/runtime/local` e injectar las páginas iniciales del crawler `./bin/nutch inject urls`.
5. Por cada iteracción que se realice para recabar datos hay que ejecutar el siguiente conjunto de comandos:

		./bin/nutch generate -topN 100
		./bin/nutch fetch -all
		./bin/nutch parse -all
		./bin/nutch updatedb -all
		
6. Introducir datos en solr desde nutch: se puede realizar con el siguiente comando `./bin/nutch solrindex http://localhost:8983/solr -all`.

Una vez introducidos los datos en solr ya se puede parar hbase mediante el comando  `./hbase-0.94.14/bin/stop-hbase.sh` desde la carpeta raiz del proyecto.


